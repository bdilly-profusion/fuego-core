tarball=fs_mark-3.2.tgz

function test_build {
    make
}

function test_deploy {
    put fs_mark  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    assert_define BENCHMARK_FS_MARK_MOUNT_BLOCKDEV
    assert_define BENCHMARK_FS_MARK_MOUNT_POINT
    assert_define BENCHMARK_FS_MARK_COUNT
    assert_define BENCHMARK_FS_MARK_SIZE

    hd_test_mount_prepare $BENCHMARK_FS_MARK_MOUNT_BLOCKDEV $BENCHMARK_FS_MARK_MOUNT_POINT

    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./fs_mark -d . -n $BENCHMARK_FS_MARK_COUNT -s $BENCHMARK_FS_MARK_SIZE"

    sync

    hd_test_clean_umount $BENCHMARK_FS_MARK_MOUNT_BLOCKDEV $BENCHMARK_FS_MARK_MOUNT_POINT
}
