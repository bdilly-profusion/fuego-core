function test_pre_check {
    assert_define FUNCTIONAL_AUTOPKGTEST_PACKAGES
    is_on_target autopkgtest PROGRAM_AUTOPKGTEST /usr/bin
    assert_define PROGRAM_AUTOPKGTEST
}

function test_run {
    report "echo 'Running tests for these packages: $FUNCTIONAL_AUTOPKGTEST_PACKAGES'"
    for PACKAGE in $FUNCTIONAL_AUTOPKGTEST_PACKAGES; do
        report_append "cd $BOARD_TESTDIR/fuego.$TESTDIR; mkdir $PACKAGE; cd $PACKAGE; \
            apt-get source $PACKAGE; cd $PACKAGE-*; autopkgtest --output-dir ./output-dir-$PACKAGE -B  . -- null; \
            echo 'Fuego test_set: $PACKAGE' >> $BOARD_TESTDIR/fuego.$TESTDIR/complete-summary.txt; \
            cat ./output-dir-$PACKAGE/summary >> $BOARD_TESTDIR/fuego.$TESTDIR/complete-summary.txt"
    done
}

function test_fetch_results {
    get $BOARD_TESTDIR/fuego.$TESTDIR/complete-summary.txt $LOGDIR/summary.txt
}
