#!/bin/python

import os, re, sys

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

AUTOPKGTST_SUMMARY = plib.LOGDIR + "/summary.txt"

print "Parsing: " + AUTOPKGTST_SUMMARY

results = {}
with open(AUTOPKGTST_SUMMARY) as f:
    for line in f:
        print line.strip()
        if line.startswith("Fuego test_set:"):
            test_set = line.split()[2]
            continue
        if line.strip():
            print line.strip()
            fields = line.split()
            test_case = fields[0]
            results[test_set+'.'+test_case] = "PASS" if fields[1] == 'PASS' else "FAIL"

sys.exit(plib.process(results))
