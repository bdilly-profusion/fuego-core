#!/usr/bin/python

import os, re, sys

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

ref_section_pat = "\[[\w]+.[gle]{2}\]"
cur_search_pat = re.compile("^(.*)(\d{2,3}\s+\d{1,2}.\d{1,2})\s+(\d{1,2}.\d{1,2}%)\s+(\d{1,2}.\d{1,3}%)\s+(\d{1,2}.\d{1,3}%)\s+(\d{1,6}.\d{1,3})(.*)", re.MULTILINE)

cur_dict = {}
pat_result = plib.parse(cur_search_pat)
sum = 0
if pat_result:
	for result in pat_result:
		sum += float(result[5])
cur_dict['linpack'] = str(sum/len(pat_result))

sys.exit(plib.process_data(ref_section_pat, cur_dict, 's', 'Avg. KFLOPS'))
