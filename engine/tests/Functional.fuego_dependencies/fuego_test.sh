
# try lots of different kinds of needs here
NEED_MEMORY="200M"
#NEED_MEMORY="1G"
#NEED_FREE_STORAGE="100M $TESTDIR"
#NEED_KCONFIG="CONFIG_PRINTK=y CONFIG_LOCKDEP_SUPPORT=n CONFIG_ARM"

function test_run {
    report "echo Dependency check should have happened already"
    report_append "echo \"ok 1 check_needs allowed script to proceed: NEED_MEMORY=$NEED_MEMORY\""

    tmpfile=$(mktemp)

    # some tests will fail
    set +e
    # now do some host-driven direct tests of check_needs
    desc="Not enough memory: NEED_MEMORY=100G"
    NEED_MEMORY=100G
    check_needs
    rcode="r$?"
    expected="r1"
    if [ $rcode == "$expected" ] ; then
      report_append "echo ok 2 - $desc : rcode=$rcode"
    else
      report_append "not ok 2 - $desc : rcode=$rcode"
    fi
    unset NEED_MEMORY

    desc="Enough storage: [NEED_FREE_STORAGE=20K $BOARD_TESTDIR]"
    NEED_FREE_STORAGE="20K $BOARD_TESTDIR"
    check_needs
    rcode="r$?"
    expected="r0"
    if [ $rcode == "$expected" ] ; then
      report_append "echo ok 3 - $desc : rcode=$rcode"
    else
      report_append "not ok 3 - $desc : rcode=$rcode"
    fi

    desc="Not enough storage: [NEED_FREE_STORAGE=5000G $TESTDIR]"
    NEED_FREE_STORAGE="5000G $BOARD_TESTDIR"
    check_needs
    rcode="r$?"
    expected="r1"
    if [ $rcode == "$expected" ] ; then
      report_append "echo ok 4 - $desc : rcode=$rcode"
    else
      report_append "echo not ok 4 - $desc : rcode=$rcode"
    fi
    unset NEED_FREE_STORAGE

    desc="Kconfig present: NEED_KCONFIG=CONFIG_PRINTK=y"
    NEED_KCONFIG="CONFIG_PRINTK=y"
    check_needs
    rcode="r$?"
    expected="r0"
    if [ $rcode == "$expected" ] ; then
      report_append "echo ok 5 - $desc : rcode=$rcode"
    else
      report_append "echo not ok 5 - $desc : rcode=$rcode"
    fi
    unset NEED_KCONFIG

    set -e
}

function test_processing {
    log_compare "$TESTDIR" "5" "^ok" "p"
}
