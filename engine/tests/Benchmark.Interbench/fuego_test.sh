tarball=interbench-0.31.tar.bz2

function test_build {
    patch -p0 < $TEST_HOME/interbench.c.patch
    make CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP"
}

function test_deploy {
	put interbench  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./interbench -L 1 || ./interbench -L 1"  
}

function test_cleanup {
	kill_procs interbench
}


