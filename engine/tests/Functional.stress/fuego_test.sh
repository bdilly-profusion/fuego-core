tarball=stress-1.0.4.tar.gz

function test_build {
    ./configure --host=$HOST --build=`uname -m`-linux-gnu CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" CFLAGS="$CFLAGS";
    make
}

function test_deploy {
	put src/stress  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	assert_define FUNCTIONAL_STRESS_SYNC
	assert_define FUNCTIONAL_STRESS_SQRT
	assert_define FUNCTIONAL_STRESS_HDD
	assert_define FUNCTIONAL_STRESS_HDD_BYTES
	assert_define FUNCTIONAL_STRESS_VM
	assert_define FUNCTIONAL_STRESS_VM_BYTES
	assert_define FUNCTIONAL_STRESS_TIMEOUT
	
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./stress -i $FUNCTIONAL_STRESS_SYNC -c $FUNCTIONAL_STRESS_SQRT --hdd $FUNCTIONAL_STRESS_HDD --hdd-bytes $FUNCTIONAL_STRESS_HDD_BYTES  --vm $FUNCTIONAL_STRESS_VM --vm-bytes $FUNCTIONAL_STRESS_VM_BYTES -t $FUNCTIONAL_STRESS_TIMEOUT"  
}

function test_processing {
	log_compare "$TESTDIR" "1" "successful run completed in" "p"
}



