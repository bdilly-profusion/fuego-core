tarball=ppp-2.4.7.tar.gz

function test_build {
    ${CC} ppp_response.c -o ppp_response
    echo "#!/bin/bash

    python vserial.py &
    sleep 1
    pwd; ls
    ./ppp_response \$(cat slave1) &
    sleep 1
    pppd connect 'chat -v \"\" AT OK ATDT5551212 CONNECT' \$(cat slave2) 38400 debug defaultroute nocrtscts idle 60 &
    sleep 5
    ifconfig -a
    if ifconfig ppp0 ; then
    echo 'TEST-1 OK'
    else
    echo 'TEST-1 FAILED'
    fi

    killall python
    killall ppp_response
#    killall pppd
    " > run-tests.sh
}

function test_deploy {
    pwd
    ls
    put * run-tests.sh  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; sh -v run-tests.sh 2>&1"
}

function test_processing {
    log_compare "$TESTDIR" "1" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}

