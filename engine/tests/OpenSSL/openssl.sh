function test_pre_check {
    assert_define "SDKROOT"
    assert_define "CC"
    assert_define "AR"
    assert_define "RANLIB"
}

function test_build {
    SUFFIX=" --sysroot=${SDKROOT}"

    # ignore the none-linux-gnueabi - it's just a dummy placeholder
    ./Configure --cross-compile-prefix=${CROSS_COMPILE} shared zlib-dynamic os/compiler:none-linux-gnueabi

    # adjust Makefiles to use our tools and our sysroot
    # some of these may already have been adjusted by Configure
    sed -i -e "s#CC= cc#CC= ${CC}#g" -e "s#^AR= ar#AR= ${AR}#g" Makefile
    sed -i -e "s#CFLAG= #CFLAG= ${SUFFIX} #g"  Makefile
    sed -i -e "s#RANLIB= ranlib#RANLIB= ${RANLIB}#g" Makefile

    sed -i -e "s#CC=		cc#CC= ${CC}#g" -e "s#CFLAG=		#CFLAG= ${SUFFIX} #g"  apps/Makefile
    sed -i -e "s#CC=		cc#CC= ${CC}#g" -e "s#AR=		ar#AR= ${AR}#g" -e "s#CFLAG=		#CFLAG= ${SUFFIX} #g" crypto/Makefile
    sed -i -e "s#CC=cc#CC= ${CC} #g"  Makefile.shared

    # Configure puts $(CROSS_COMPILE) and value of $CROSS_COMPILE in CC
    # definition - fix that by removing the literal value
    sed -i -e "s#CC= \([$](CROSS_COMPILE)\)${CROSS_COMPILE}#CC= \1#" Makefile

    make
    echo '#!/bin/bash
    cd test
    ../util/opensslwrap.sh version -a
    ../util/shlib_wrap.sh ./bftest
    ../util/shlib_wrap.sh ./bntest
    ../util/shlib_wrap.sh ./casttest
    ../util/shlib_wrap.sh ./destest
    ../util/shlib_wrap.sh ./dhtest
    ../util/shlib_wrap.sh ./dsatest
    ../util/shlib_wrap.sh ./dummytest
    ../util/shlib_wrap.sh ./ecdhtest
    ../util/shlib_wrap.sh ./ecdsatest
    ../util/shlib_wrap.sh ./ectest
    ../util/shlib_wrap.sh ./enginetest
    ../util/shlib_wrap.sh ./evp_test evptests.txt
    ../util/shlib_wrap.sh ./exptest
    ../util/shlib_wrap.sh ./fips_dssvs
    ../util/shlib_wrap.sh ./fips_test_suite
    ../util/shlib_wrap.sh ./hmactest
    ../util/shlib_wrap.sh ./ideatest
    ../util/shlib_wrap.sh ./igetest
    ../util/shlib_wrap.sh ./md2test
    ../util/shlib_wrap.sh ./md4test
    ../util/shlib_wrap.sh ./md5test
    ../util/shlib_wrap.sh ./randtest
    ../util/shlib_wrap.sh ./rc2test
    ../util/shlib_wrap.sh ./rc4test
    ../util/shlib_wrap.sh ./rmdtest
    ../util/shlib_wrap.sh ./rsa_test
    ../util/shlib_wrap.sh ./sha1test
    ../util/shlib_wrap.sh ./sha256t
    ../util/shlib_wrap.sh ./sha512t
    ../util/shlib_wrap.sh ./shatest
    ../util/shlib_wrap.sh ./ssltest' > run-tests.sh
}
