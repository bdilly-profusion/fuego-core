tarball=iozone3_353.tar

function test_build {
    cd src/current

    if [ "$ARCHITECTURE" = "ia32" ] ; then
        TARGET=linux
    elif [ "$ARCHITECTURE" = "arm" ] ; then
        TARGET=linux-arm
    elif [ "$ARCHITECTURE" = "arm64" ] ; then
        TARGET=linux-arm
    elif [ "$ARCHITECTURE" = "x86_64" ] ; then
        TARGET=linux-AMD64
    else
        echo "platform based on $ARCHITECTURE is not supported by benchmark"
        return 1
    fi

    make $TARGET GCC="$CC" CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP"
}

function test_deploy {
    cd src/current
    put fileop iozone pit_server $BOARD_TESTDIR/fuego.$TESTDIR/
}

# function test_run {
#     report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./iozone -a -i 0 -i 1 -i 2 -i 6 -i 7 -i 8 -i 9 -O -R -g $file_size" $BOARD_TESTDIR/fuego.$TESTDIR/$TESTDIR_tmp.log
#     safe_cmd "cd $BOARD_TESTDIR/fuego.$TESTDIR; cat $BOARD_TESTDIR/fuego.$TESTDIR/$TESTDIR_tmp.log | tail -n 139 | tee  "
# }

function test_run {
    assert_define BENCHMARK_IOZONE_MOUNT_BLOCKDEV
    assert_define BENCHMARK_IOZONE_MOUNT_POINT
    assert_define BENCHMARK_IOZONE_FILE_SIZE
    assert_define BENCHMARK_IOZONE_TESTS

    hd_test_mount_prepare $BENCHMARK_IOZONE_MOUNT_BLOCKDEV $BENCHMARK_IOZONE_MOUNT_POINT

    report "cd $BENCHMARK_IOZONE_MOUNT_POINT; $BOARD_TESTDIR/fuego.$TESTDIR/iozone -a $BENCHMARK_IOZONE_TESTS -O -R -b $BOARD_TESTDIR/fuego.$TESTDIR/results.${BUILD_ID}.${BUILD_NUMBER}.xlsx -g $BENCHMARK_IOZONE_FILE_SIZE" $BOARD_TESTDIR/fuego.$TESTDIR/Benchmark.IOzone.log
    get $BOARD_TESTDIR/fuego.$TESTDIR/results.${BUILD_ID}.${BUILD_NUMBER}.xlsx ${LOGDIR}/results.xlsx
    safe_cmd "cd $BOARD_TESTDIR/fuego.$TESTDIR; cat $BOARD_TESTDIR/fuego.$TESTDIR/Benchmark.IOzone.log | tail -n 139 | tee  "

    hd_test_clean_umount $BENCHMARK_IOZONE_MOUNT_BLOCKDEV $BENCHMARK_IOZONE_MOUNT_POINT
}

function test_cleanup {
    kill_procs iozone
}


