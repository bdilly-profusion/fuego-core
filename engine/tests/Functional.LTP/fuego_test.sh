# FIXTHIS: use https://github.com/linux-test-project/ltp.git instead of a tarball
tarball=ltp-7eb77fbfd80.tar.bz2

ALLTESTS="
admin_tools         fs_perms_simple       ltp-aiodio.part3  net_stress.appl                   quickhit
can                 fs_readonly           ltp-aiodio.part4  net_stress.broken_ip              rpc_tests
cap_bounds          fsx                   ltplite           net_stress.interfaces             sched
commands            hugetlb               lvm.part1         net_stress.ipsec_icmp             scsi_debug.part1
connectors          hyperthreading        lvm.part2         net_stress.ipsec_tcp              securebits
containers          ima                   math              net_stress.ipsec_udp              smack
controllers         input                 mm                net_stress.multicast              stress.part1
cpuhotplug          io                    modules           net_stress.route                  stress.part2
crashme             io_cd                 net.features      network_commands                  stress.part3
dio                 io_floppy             net.ipv6          nptl                              syscalls
dma_thread_diotest  ipc                   net.ipv6_lib      numa                              syscalls-ipc
fcntl-locktests     kernel_misc           net.multicast     nw_under_ns                       timers
filecaps            ltp-aio-stress.part1  net.nfs           pipes                             tirpc_tests
fs                  ltp-aio-stress.part2  net.rpc           power_management_tests            tpm_tools
fs_bind             ltp-aiodio.part1      net.sctp          power_management_tests_exclusive  tracing
fs_ext4             ltp-aiodio.part2      net.tcp_cmds      pty"

ALLPTSTESTS="AIO MEM MSG SEM SIG THR TMR TPS"

ALLRTTESTS="
perf/latency
func/measurement
func/hrtimer-prio
func/gtod_latency
func/periodic_cpu_load
func/pthread_kill_latency
func/sched_football
func/pi-tests
func/thread_clock
func/rt-migrate
func/matrix_mult
func/prio-preempt
func/prio-wake
func/pi_perf
func/sched_latency
func/async_handler
func/sched_jitter
"

# OK - the logic here is a bit complicated
# We support several different usage scenarios:
#   1: fuego builds, deploys and installs LTP
#   2: fuego runs an existing installation of LTP
#   3: fuego builds and deploys LTP, to a special location
#      - this is a one-time operation
#      - this sets up the system for 2
#      - this is implemented by spec: "install"
#   4: fuego builds LTP, but deploy is left to user (manual)
#      - this creates a tarball that the user can deploy
#      - this is a one-time operation
#      - this set up the system for 2
#      - this is implemented by spec: "make_pkg"
#
# variables used:
# from board file or spec:
#   FUNCTIONAL_LTP_HOMEDIR = persistent location for LTP on board
#   FUNCTIONAL_LTP_PHASES = list of phases to force doing
#     - include string 'build' to do a build
#     - include string 'deploy' to do a deploy
#     - include string 'maketar' to do create a tarball for the target
#     - include string 'run' to do a run
#     - if empty, defaults to "build deploy run"
#
# scenario 1:
#  FUNCTIONAL_LTP_HOMEDIR="", FUNTIONAL_LTP_PHASES=""
# scenario 2:
#  FUNCTIONAL_LTP_HOMEDIR="/opt/ltp", FUNCTIONAL_LTP_PHASES=""
#  if not pre-installed, it's an error (detected in pre_check)
# scenario 3:
#  FUNCTIONAL_LTP_HOMEDIR="/opt/ltp", FUNCTIONAL_LTP_PHASES="build deploy"
#  if already pre-installed, it's an error (or rm -rf is done in deploy)
# scenario 4:
#  FUNCTIONAL_LTP_HOMEDIR="/opt/ltp", FUNCTIONAL_LTP_PHASES="build maketar"
#  user should copy tar to board, and install it, then set board file's
#    FUNCTIONAL_LTP_HOMEDIR var

function test_pre_check {
    if [ -n "${FUNCTIONAL_LTP_HOMEDIR}" ] ; then
        # user or spec has specified a home directory for LTP.
        # check to see if runltp is installed there
        is_on_target runltp PROGRAM_RUNLTP $FUNCTIONAL_LTP_HOMEDIR

        # if we're not doing a build, then not having LTP installed
        # is a problem
        if [[ "$FUNCTIONAL_LTP_PHASES" != *build* ]] ; then
            assert_define PROGRAM_RUNLTP "Expected LTP to be present on board in $FUNCTIONAL_LTP_HOMEDIR, but it's not there!"
        fi
        LTP_DESTDIR=$FUNCTIONAL_LTP_HOMEDIR

        if [ -z "${FUNCTIONAL_LTP_PHASES}" ] ;  then
            # need to include deploy, because some Fuego-specific files
            # must be deployed, even if rest of LTP is already present
            FUNCTIONAL_LTP_PHASES="deploy run"
        fi
    else
        LTP_DESTDIR=$BOARD_TESTDIR/fuego.$TESTDIR

        if [ -z "${FUNCTIONAL_LTP_PHASES}" ] ;  then
            FUNCTIONAL_LTP_PHASES="build deploy run"
        fi
    fi

    assert_define SDKROOT
    assert_define CC
    assert_define AR
    assert_define RANLIB
    assert_define LDFLAGS
    assert_define FUNCTIONAL_LTP_TESTS
}

function test_build {
    if [[ "$FUNCTIONAL_LTP_PHASES" == *build* ]] ; then
        echo "Building LTP"
        patch -p1 -N -s < $TEST_HOME/shorter-fork13.patch

        # Build the LTP tests
        make autotools
        ./configure CC="${CC}" AR="${AR}" RANLIB="${RANLIB}" LDFLAGS="$LDFLAGS" SYSROOT="${SDKROOT}" \
            --with-open-posix-testsuite --with-realtime-testsuite --without-perl --without-python --target=$PREFIX --host=$PREFIX \
            --prefix=`pwd`/target_bin --build=`uname -m`-unknown-linux-gnu

        # save build results in build.log
        set -o pipefail
        make CC="${CC}" 2>&1 | tee build.log
        set +o pipefail

        make install

        cp --parents testcases/realtime/scripts/setenv.sh target_bin
        cp $TEST_HOME/ltp_target_run.sh target_bin
    else
        echo "Skipping LTP build"
    fi
}

function test_deploy {
    # generate the skip list for this test
    echo "# skip these test cases" > skiplist.txt
    if [ -n "${FUNCTIONAL_LTP_SKIPLIST}" ]; then
        for item in $FUNCTIONAL_LTP_SKIPLIST; do
            echo $item >> skiplist.txt
        done
    fi

    if [[ "$FUNCTIONAL_LTP_PHASES" == *deploy* ]] && [[ -z "$PROGRAM_RUNLTP" ]] ; then
        echo "Deploying LTP"

        # make a staging area
        cp -r target_bin ltp

        # the syscalls group occupies by far the largest space so remove it if unneeded
        cp_syscalls=$(echo $FUNCTIONAL_LTP_TESTS | awk '{print match($0,"syscalls|quickhit|ltplite|stress.part")}')
        if [ $cp_syscalls -eq 0 ]; then
            echo "Removing syscalls binaries"
            awk '/^[^#]/ { print "rm -f ltp/testcases/bin/" $2 }' ltp/runtest/syscalls | sh
        fi

        cp skiplist.txt ltp

        put ltp/* $LTP_DESTDIR
        rm -rf ltp
    else
        echo "Skipping LTP deploy to board"
    fi

    if [[ "$FUNCTIONAL_LTP_PHASES" == *maketar* ]] ; then
        cp -r target_bin ltp
        echo "Creating LTP binary tarball"
        tar zcpf ltp.tar.gz ltp/
        mv ltp.tar.gz ${LOGDIR}/
        echo "LTP tar file ltp.tar.gz is available in ${LOGDIR}, for manual deployment"
    fi

    # pre-installed LTP needs a few fuego-specific items
    if [ -n "$FUNCTIONAL_LTP_HOMEDIR" ] ; then
       cmd "mkdir -p $FUNCTIONAL_LTP_HOMEDIR/testcases/realtime/scripts"
       put testcases/realtime/scripts/setenv.sh $FUNCTIONAL_LTP_HOMEDIR/testcases/realtime/scripts
       put $TEST_HOME/ltp_target_run.sh $FUNCTIONAL_LTP_HOMEDIR
       put skiplist.txt $FUNCTIONAL_LTP_HOMEDIR
    fi
}

function test_run {
    if [[ "$FUNCTIONAL_LTP_PHASES" == *run* ]] ; then
        echo "Running LTP"

        TESTS=""
        PTSTESTS=""
        RTTESTS=""

        # ltp_target_run.sh invokes the test different ways, depending
        # on the type of the test (regular, posize, or realtime)
        # Separate the test list by type, and pass the tests in different
        # variables
        for a in $FUNCTIONAL_LTP_TESTS; do
            for b in $ALLTESTS; do
                if [ "$a" == "$b" ]; then
                    TESTS+="$a "
                fi
            done

            for b in $ALLPTSTESTS; do
                if [ "$a" == "$b" ]; then
                    PTSTESTS+="$a "
                fi
            done

            for b in $ALLRTTESTS; do
                if [ "$a" == "$b" ]; then
                    RTTESTS+="$a "
                fi
            done
        done

        # Let some of the tests fail, the information will be in the result xlsx file
        report "cd $LTP_DESTDIR; TESTS=\"$TESTS\"; PTSTESTS=\"$PTSTESTS\"; RTTESTS=\"$RTTESTS\"; . ./ltp_target_run.sh"
    else
        echo "Skipping LTP run"

        # for a build-only test, put the build results into the testlog 
        # (via roundtrip to board - pretty wasteful, but oh well...)
        put build.log $BOARD_TESTDIR/fuego.$TESTDIR
        report "cat $BOARD_TESTDIR/fuego.$TESTDIR/build.log"

        report_append "echo 'OK - LTP run skipped.'"
    fi
}

function test_fetch_results {
    if [[ "$FUNCTIONAL_LTP_PHASES" == *run* ]] ; then
        echo "Fetching LTP results"
        rm -rf result/
        get $LTP_DESTDIR/result $LOGDIR
    else
        echo "Skip fetching LTP run results"
    fi
}

function test_processing {
    if [[ "$FUNCTIONAL_LTP_PHASES" == *run* ]] ; then
        echo "Processing LTP results"
        SAVEDIR=$(pwd)
        cd ${LOGDIR}/result/
        # Restore the path so that python works properly
        export PATH=$ORIG_PATH
        cp $TEST_HOME/ltp_process.py .
        python ltp_process.py
        [ -e results.xlsx ] && cp results.xlsx ${LOGDIR}/results.xlsx
        [ -e rt.log ] && cp rt.log ${LOGDIR}
        cd ${SAVEDIR}
    else
        echo "Processing LTP build log"
        log_compare "$TESTDIR" "1836" "compile PASSED$" "p"
    fi
}
