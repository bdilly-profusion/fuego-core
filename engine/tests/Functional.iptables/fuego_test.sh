function test_run {
	report " \
	if	iptables -A INPUT -t filter -p tcp --dport 80 -j DROP; then \
			echo 'TEST-1 OK'; \
		else \
			echo 'TEST-1 FAIL'; \
		fi; \
	if	iptables -t mangle -A PREROUTING -p tcp --dport 80 -j TOS --set-tos Maximize-Throughput; then \
			echo 'TEST-2 OK'; \
		else \
			echo 'TEST-2 FAIL'; \
		fi; \
	if	iptables -t raw -I PREROUTING -p udp --dport 53 -j NOTRACK &&
		iptables -t raw -I OUTPUT -p udp --dport 53 -j NOTRACK; then \
			echo 'TEST-3 OK'; \
		else \
			echo 'TEST-3 FAIL'; \
		fi; \
	if	iptables -N dropchain && \
		iptables -A dropchain -j DROP && \
		iptables -A INPUT -p udp --dport 80 -j dropchain; then \
			echo 'TEST-4 OK'; \
		else \
			echo 'TEST-4 FAIL'; \
		fi; \
	if	iptables -A INPUT -d 192.168.3.100 -j DROP; then \
			echo 'TEST-5 OK'; \
		else \
			echo 'TEST-5 FAIL'; \
		fi; \
	if	iptables -A INPUT -p tcp --tcp-flags SYN,RST,ACK SYN --dport 80 -j ACCEPT; then \
			echo 'TEST-6 OK'; \
		else \
			echo 'TEST-6 FAIL'; \
		fi; \
	if	iptables-save >> iptables_before.log; then \
			echo 'TEST-7 OK'; \
		else \
			echo 'TEST-7 FAIL'; \
		fi; \
	if	iptables -t filter -D INPUT 2; then \
			echo 'TEST-8 OK'; \
		else \
			echo 'TEST-8 FAIL'; \
		fi; \
	if	iptables-save >> iptables_after.log; then  \
			echo 'TEST-9 OK'; \
		else \
			echo 'TEST-9 FAIL'; \
		fi \
	"
}

function test_processing {
	P_CRIT="TEST.*OK"
	N_CRIT="TEST.*FAIL"

	log_compare "$TESTDIR" "9" "${P_CRIT}" "p"
	log_compare "$TESTDIR" "0" "${N_CRIT}" "n"
}




