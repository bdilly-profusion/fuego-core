# this is a bit of a stub, but it's a start
# FIXTHIS - the test program requires perl (it should be converted to C)
#  - unless our intention is to test perl and C libraries

function test_pre_check {
    is_on_target perl PROGRAM_PERL /usr/bin
    assert_define PROGRAM_PERL
}

function test_deploy {
    put $TEST_HOME/year2038-test.pl  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./year2038-test.pl"
}

function test_processing {
    log_compare "$TESTDIR" "1" "1901" "n"
}


