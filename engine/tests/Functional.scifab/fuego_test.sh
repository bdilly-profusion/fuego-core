tarball=dung-3.4.25-m2.tar.gz

function test_deploy {
    put ./* $OSV_HOME/osv.$TESTDIR/
}

function test_run {
    report "cd $OSV_HOME/osv.$TESTDIR/scifab; ./dmesg-sh-sci.0.sh; ./proc-interrupts.sh"
}

function test_processing {
    assert_define FUNCTIONAL_SCIFAB_RES_LINES_COUNT
    assert_define FUNCTIONAL_SCIFAB_RES_PASS_COUNT
    assert_define FUNCTIONAL_SCIFAB_RES_FAIL_COUNT

    check_capability "RENESAS"

    log_compare "$TESTDIR" $FUNCTIONAL_SCIFAB_RES_LINES_COUNT "Passed:$FUNCTIONAL_SCIFAB_RES_PASS_COUNT Failed:$FUNCTIONAL_SCIFAB_RES_FAIL_COUNT" "p"
}


