tarball=libcroco-0.6.11.tar.gz

function test_build {
    echo " test_build croco.sh "
    libtoolize --automake
    aclocal --system-acdir=${SDKROOT}usr/share/aclocal
    autoreconf --verbose --install --force --exclude=autopoint
    autoconf
    autoheader
    automake -a
    ./configure --host=$PREFIX --enable-Bsymbolic=auto
    make
    mkdir -p test_croco
    cp tests/.libs/* test_croco
    cp tests/test-inputs/test*.css test_croco
    tar czvf croco_test_libs.tar.gz test_croco
}

function test_deploy {
    put croco_test_libs.tar.gz $BOARD_TESTDIR/fuego.$TESTDIR/;
    put $TEST_HOME/croco_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/;
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; sh -v croco_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "10" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAIL" "n"
}


