#!/bin/bash

cd $BOARD_TESTDIR/fuego.$TESTDIR
tar xzvf croco_test_libs.tar.gz
cd test_croco && ls -l
if csslint-0.6 -v; then echo -e '\nTEST-1 OK'; else echo -e '\nTEST-1 FAIL'; fi;
if csslint-0.6 test1.css; then echo -e '\nTEST-2 OK'; else echo -e '\nTEST-2 FAIL'; fi;
if csslint-0.6 --dump-location test1.css; then echo -e '\nTEST-3 OK'; else echo -e '\nTEST-3 FAIL'; fi;
if ./test0 test0.1.css; then echo -e '\nTEST-4 OK'; else echo -e '\nTEST-4 FAIL'; fi;
if ./test1 test1.css; then echo -e '\nTEST-5 OK'; else echo -e '\nTEST-5 FAIL'; fi;
if ./test2 test2.1.css; then echo -e '\nTEST-6 OK'; else echo -e '\nTEST-6 FAIL'; fi;
if ./test3 test3.css; then echo -e '\nTEST-7 OK'; else echo -e '\nTEST-7 FAIL'; fi;
if ./test4 test4.1.css; then echo -e '\nTEST-8 OK'; else echo -e '\nTEST-8 FAIL'; fi;
if ./test5 test5.1.css; then echo -e '\nTEST-9 OK'; else echo -e '\nTEST-9 FAIL'; fi;
if ./test6 test5.1.css; then echo -e '\nTEST-10 OK'; else echo -e '\nTEST-10 FAIL'; fi;
echo "test end .............."
