#!/usr/bin/python

import os, re, sys

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

measurements = {}

print "Reading current values from " + plib.TEST_LOG + "\n"
with open(plib.TEST_LOG,'r') as cur_file:
    raw_values = cur_file.readlines()
    results = raw_values[-1].rstrip("\n").split(",")

if len(results) < 26:
    print "\nFuego error reason: No results found\n"
    sys.exit(2)

print "Bonnie++ raw results: " + str(results)


measurements["Sequential_Output.PerChr"] = []
measurements["Sequential_Output.Block"] = []
measurements["Sequential_Output.Rewrite"] = []
measurements["Sequential_Input.PerChr"] = []
measurements["Sequential_Input.Block"] = []
measurements["Random.Seeks"] = []
measurements["Sequential_Create.Create"] = []
measurements["Sequential_Create.Read"] = []
measurements["Sequential_Create.Delete"] = []
measurements["Random_Create.Create"] = []
measurements["Random_Create.Read"] = []
measurements["Random_Create.Delete"] = []

if not '+' in results[2]:
    measurements["Sequential_Output.PerChr"].append({"name": "speed", "measure" : float(results[2])})
if not '+' in results[3]:
    measurements["Sequential_Output.PerChr"].append({"name": "CPU", "measure" : float(results[3])})
if not '+' in results[4]:
    measurements["Sequential_Output.Block"].append({"name": "speed", "measure" : float(results[4])})
if not '+' in results[5]:
    measurements["Sequential_Output.Block"].append({"name": "CPU", "measure" : float(results[5])})
if not '+' in results[6]:
    measurements["Sequential_Output.Rewrite"].append({"name": "speed", "measure" : float(results[6])})
if not '+' in results[7]:
    measurements["Sequential_Output.Rewrite"].append({"name": "CPU", "measure" : float(results[7])})
if not '+' in results[8]:
    measurements["Sequential_Input.PerChr"].append({"name": "speed", "measure" : float(results[8])})
if not '+' in results[9]:
    measurements["Sequential_Input.PerChr"].append({"name": "CPU", "measure" : float(results[9])})
if not '+' in results[10]:
    measurements["Sequential_Input.Block"].append({"name": "speed", "measure" : float(results[10])})
if not '+' in results[11]:
    measurements["Sequential_Input.Block"].append({"name": "CPU", "measure" : float(results[11])})
if not '+' in results[12]:
    measurements["Random.Seeks"].append({"name": "speed", "measure" : float(results[12])})
if not '+' in results[13]:
    measurements["Random.Seeks"].append({"name": "CPU", "measure" : float(results[13])})
if not '+' in results[15]:
    measurements["Sequential_Create.Create"].append({"name": "speed", "measure" : float(results[15])})
if not '+' in results[16]:
    measurements["Sequential_Create.Create"].append({"name": "CPU", "measure" : float(results[16])})
if not '+' in results[17]:
    measurements["Sequential_Create.Read"].append({"name": "speed", "measure" : float(results[17])})
if not '+' in results[18]:
    measurements["Sequential_Create.Read"].append({"name": "CPU", "measure" : float(results[18])})
if not '+' in results[19]:
    measurements["Sequential_Create.Delete"].append({"name": "speed", "measure" : float(results[19])})
if not '+' in results[20]:
    measurements["Sequential_Create.Delete"].append({"name": "CPU", "measure" : float(results[20])})
if not '+' in results[21]:
    measurements["Random_Create.Create"].append({"name": "speed", "measure" : float(results[21])})
if not '+' in results[22]:
    measurements["Random_Create.Create"].append({"name": "CPU", "measure" : float(results[22])})
if not '+' in results[23]:
    measurements["Random_Create.Read"].append({"name": "speed", "measure" : float(results[23])})
if not '+' in results[24]:
    measurements["Random_Create.Read"].append({"name": "CPU", "measure" : float(results[24])})
if not '+' in results[25]:
    measurements["Random_Create.Delete"].append({"name": "speed", "measure" : float(results[25])})
if not '+' in results[26]:
    measurements["Random_Create.Delete"].append({"name": "CPU", "measure" : float(results[26])})

sys.exit(plib.process(measurements))
