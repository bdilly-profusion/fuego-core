function test_build {
	echo " test_build_kmod.sh " 	
}

function test_deploy {
	echo " test_deploy_kmod.sh "
}

function test_run {
	report " \
	if lsmod | grep 'rfcomm' && rmmod rfcomm.ko; then echo 'Prepare for Test-1'; fi;\
	if modinfo -n rfcomm|xargs insmod; then echo 'TEST-1 OK'; else echo 'TEST-1 FAILED'; fi;\
	if lsmod | grep 'rfcomm' && echo 'rfcomm is loaded' ; then echo 'TEST-2 OK'; else echo 'TEST-2 FAILED'; fi;\
	if modinfo rfcomm; then echo 'TEST-3 OK'; else echo 'TEST-3 FAILED'; fi;\
	if rmmod rfcomm.ko; then echo 'TEST-4 OK'; else echo 'TEST-4 FAILED'; fi;\
	if lsmod | grep 'rfcomm' || echo 'rfcomm.ko is removed'; then echo 'TEST-5 OK'; else echo 'TEST-5 FAILED'; fi;\
	if depmod -w ; then echo 'TEST-6 OK'; else echo 'TEST-6 FAILED'; fi;\
	if modprobe rfcomm; then echo 'TEST-7 OK'; else echo 'TEST-7 FAILED'; fi;\
	if lsmod | grep 'rfcomm' && echo 'rfcomm is loaded' ; then echo 'TEST-8 OK'; else echo 'TEST-8 FAILED'; fi;\
	if kmod list | grep 'rfcomm' && echo 'rfcomm is loaded check by kmodlist' ; then echo 'TEST-9 OK'; else echo 'TEST-9 FAILED'; fi"
}

function test_processing {
	log_compare "$TESTDIR" "9" "^TEST.*OK" "p"
    	log_compare "$TESTDIR" "0" "^TEST.*FAILED" "n"
}




