#tarball=jpeg-6b.tar.gz
tarball=images.tar.gz

function test_build {
#    ./configure --host=$PREFIX --build=`./config.guess` CC="$CC" AR="$AR" RANLIB="$RANLIB"
#    make
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
    <image>
        <read filename=\"button.gif\" />
        <get width=\"base-width\" height=\"base-height\" />
        <resize geometry=\"%[dimensions]\" />
        <get width=\"resize-width\" height=\"resize-height\" />
        <print output=\"Image sized from %[base-width]x%[base-height] to %[resize-width]x%[resize-height].\n\" />
        <write filename=\"button.png\" />
    </image>" > incantation.msl

    echo "#!/bin/bash
    if convert rose.jpg -sharpen 0x1 reconstruct.jpg; then echo 'TEST-1 OK'; else echo 'TEST-1 FAIL'; fi;
    compare rose.jpg reconstruct.jpg difference1.png; if [ -f difference1.png ]; then echo 'TEST-2 OK'; else echo 'TEST-2 FAIL'; fi;
    compare -compose src rose.jpg reconstruct.jpg difference2.png; if [ -f difference2.png ]; then echo 'TEST-3 OK'; else echo 'TEST-3 FAIL'; fi;
    if mogrify -resize 50% rose.jpg; then echo 'TEST-4 OK'; else echo 'TEST-4 FAIL'; fi;
    if mogrify -resize 256x256 *.jpg; then echo 'TEST-5 OK'; else echo 'TEST-5 FAIL'; fi;
    if mogrify -format jpg *.png; then echo 'TEST-6 OK'; else echo 'TEST-6 FAIL'; fi;
    if composite -gravity center smile.gif  rose: rose-over.png; then echo 'TEST-7 OK'; else echo 'TEST-7 FAIL'; fi;
    if convert -size 70x70 canvas:none -fill red -draw 'circle 35,35 10,30' red-circle.png; then echo 'TEST-8 OK'; else echo 'TEST-8 FAIL'; fi;
    if convert -size 70x70 canvas:none -draw 'circle 35,35 35,20' -negate -channel a -gaussian-blur 0x8 white-highlight.png; then echo 'TEST-9 OK'; else echo 'TEST-9 FAIL'; fi;
    if identify -verbose rose.jpg; then echo 'TEST-10 OK'; else echo 'TEST-10 FAIL'; fi;
    if composite -compose atop -geometry -13-17 white-highlight.png red-circle.png red-ball.png; then echo 'TEST-11 OK'; else echo 'TEST-11 FAIL'; fi;
    conjure -dimensions 400x400 incantation.msl; if [ -f button.png ]; then echo 'TEST-12 OK'; else echo 'TEST-12 FAIL'; fi;
    if montage -background '#336699' -geometry +4+4 rose.jpg red-ball.png montage.jpg; then echo 'TEST-13 OK'; else echo 'TEST-13 FAIL'; fi;
    if montage -label %f -frame 5 -background '#336699' -geometry +4+4 rose.jpg red-ball.png frame.jpg; then echo 'TEST-14 OK'; else echo 'TEST-14 FAIL'; fi;
    if convert rose.jpg rose.png; then echo 'TEST-15 OK'; else echo 'TEST-15 FAIL'; fi;
    if convert rose.jpg -resize 50% rose.png; then echo 'TEST-16 OK'; else echo 'TEST-16 FAIL'; fi;
    if convert -size 640x480 canvas:none -fill red -draw \"circle 320,240 320,100\" fuzzy-magick.png; then echo 'TEST-17 OK'; else echo 'TEST-17 FAIL'; fi;
    if convert red-ball.png -colorspace rgb +sigmoidal-contrast 11.6933 \
      -define filter:filter=sinc -define filter:window=jinc -define filter:lobes=3 \
      -resize 400% -sigmoidal-contrast 11.6933 -colorspace srgb output.png; then echo 'TEST-18 OK'; else echo 'TEST-18 FAIL'; fi;
    if stream -map rgb -storage-type char rose.jpg pixels.dat; then echo 'TEST-19 OK'; else echo 'TEST-19 FAIL'; fi;
    if convert rose.jpg image.tif; stream -map i -storage-type double -extract 100x100+30+40 image.tif gray.raw; then echo 'TEST-20 OK'; else echo 'TEST-20 FAIL'; fi;
    if stream -map i -storage-type double 'image.tif[100x100+30+40]' gray.raw; then echo 'TEST-21 OK'; else echo 'TEST-21 FAIL'; fi;
    " > run-tests.sh

#    echo "#!/bin/bash
#    if ./djpeg -dct int -ppm -outfile testout.ppm testorig.jpg; then echo 'TEST-1 OK'; else echo 'TEST-1 FAIL'; fi;
#    " > run-tests.sh

}

function test_deploy {
    put run-tests.sh *.jpg *.png *.gif incantation.msl $BOARD_TESTDIR/fuego.$TESTDIR/;
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR; sh -v run-tests.sh"  
}

function test_processing {
    log_compare "$TESTDIR" "21" "^TEST.*OK" "p"
    log_compare "$TESTDIR" "0" "^TEST.*FAIL" "n"
}


