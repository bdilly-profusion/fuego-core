function test_run {
    report "echo \"1..6\" ; \
        echo \"ok 1 test_set1.test_case1\" ; \
        echo \"ok 2 test_set1.test_case2\" ; \
        echo \"ok 3 test_multi.part2.test_case3\" ; \
        echo \"ok 4 test_multi.part2.test_case4\" ; \
        echo \"ok 5 test_multi.part2.p3.test_case5\" ; \
        echo \"ok 6 test_multi.part2.p3.test_case6\""
}

function test_processing {
    log_compare $TESTDIR "^ok" p 4
}
