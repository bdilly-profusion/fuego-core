#!/usr/bin/python
# See common.py for description of command-line arguments

import os, sys

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

results = {}

regex_string = '^(ok|not ok) (\d+) (.*)$'
matches = plib.parse_log(regex_string)

for m in matches:
    print("DEBUG: in parser.py: m=%s" % str(m))
    status = m[0]
    test_num = m[1]
    test_id = m[2].replace(" ",".")
    results[test_id] = 'PASS' if status == 'ok' else 'FAIL'

sys.exit(plib.process(results))
