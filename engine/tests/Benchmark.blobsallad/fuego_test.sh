tarball=blobsallad-src.tar.bz2

function test_pre_check {
    is_on_target xrandr XRANDR_PROGRAM /usr/bin
    assert_define XRANDR_PROGRAM
    # should check for SDL and cairo
    is_on_sdk SDL.h SDL_HEADER /usr/include
    assert_define SDL_HEADER
}

function test_build {
    patch -p0 -N -s < $TEST_HOME/blobsallad.Makefile.patch
    patch -p0 -N -s < $TEST_HOME/blobsallad.auto.patch
    patch -p0 -N -s < $TEST_HOME/bs_main.c.patch

    make CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" SDKROOT="$SDKROOT" 
}

function test_deploy {
	put blobsallad  $BOARD_TESTDIR/fuego.$TESTDIR/
	put maps  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; export DISPLAY=:0; xrandr | awk '/\*/ {split(\$1, a, \"x\"); exit(system(\"./blobsallad \" a[1]  a[2]))}'"
}


