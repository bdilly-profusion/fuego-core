export GOPATH=${WORKSPACE}/vuls-shared/go/
export GOROOT=/usr/local/go
export PATH=$PATH:${GOROOT}/bin:${GOPATH}/bin

function test_pre_check {
    # VULS required an ssh connection without password
    assert_define SSH_KEY "Missing file definition with ssh private key"
    if [ "${TRANSPORT}" != "ssh" ]; then
        abort_job "Vuls requires your board to define TRANSPORT to be ssh"
    fi

    # FIXTHIS: support other distributions (e.g. CentOS requires yum-utils)
    if [ "${BENCHMARK_VULS_DISTRO}" == "debian" ]; then
        is_on_target reboot-notifier SERVICE_REBOOT_NOTIFIER /etc/cron.daily/:/etc/default
        assert_define SERVICE_REBOOT_NOTIFIER "Missing reboot-notifier package on target"
        if [ "${BENCHMARK_VULS_ISDEEP}" == "true" ] ; then
            is_on_target aptitude-curses PROGRAM_APTITUDE /usr/bin
            assert_define PROGRAM_APTITUDE "Missing aptitude package on target"
        fi
    fi

    # vuls uses strict checking and jenkins has no home directory so use /etc
    if [ -e /etc/ssh/ssh_known_hosts ]; then
        grep ${IPADDR} /etc/ssh/ssh_known_hosts || \
            abort_job "Run on docker as root: ssh-keyscan -t ecdsa ${IPADDR} >> /etc/ssh/ssh_known_hosts"
    else
        abort_job "Run on docker as root: ssh-keyscan -t ecdsa ${IPADDR} >> /etc/ssh/ssh_known_hosts"
    fi

    if [ "${BENCHMARK_VULS_ISDEEP}" == "true" ]; then
        if check_root ; then
            abort_job "You need to login as root for vuls deep scan"
        fi
    fi

    # Make sure that go was installed in the system
    is_on_target go PROGRAM_GO $PATH
    assert_define PROGRAM_GO "run /fuego-ro/scripts/fuego-vuls-docker-preparation"
}

function test_build {
    if [ ! -d ${GOPATH}/src/github.com/kotakanbe/ ]; then
        # Install go-cve-dictionary into ${GOPATH}/bin
        mkdir -p ${GOPATH}/src/github.com/kotakanbe
        cd ${GOPATH}/src/github.com/kotakanbe
        git clone https://github.com/kotakanbe/go-cve-dictionary.git
        cd go-cve-dictionary && make install

        # Install goval-dictionary into ${GOPATH}/bin
        cd ${GOPATH}/src/github.com/kotakanbe
        git clone https://github.com/kotakanbe/goval-dictionary.git
        cd goval-dictionary && make install
    fi

    # Install Vuls into ${GOPATH}/bin
    if [ ! -d $GOPATH/src/github.com/future-architect/ ]; then
        mkdir -p $GOPATH/src/github.com/future-architect/
        cd $GOPATH/src/github.com/future-architect/
        git clone https://github.com/future-architect/vuls.git
        cd $GOPATH/src/github.com/future-architect/vuls
        make install
    fi

    # Share the database creation on a common directory
    cd $GOPATH/src/github.com/future-architect/vuls

    # Fetch vulnerability data from NVD and OVAL data
    if [ -n "${http_proxy}" ]; then
        for i in `seq 2002 $(date +"%Y")`; do
            go-cve-dictionary fetchnvd -http-proxy=$http_proxy -years $i
        done
        goval-dictionary fetch-${BENCHMARK_VULS_DISTRO} ${BENCHMARK_VULS_VERSIONS} -http-proxy ${http_proxy}
    else
        for i in `seq 2002 $(date +"%Y")`; do
            go-cve-dictionary fetchnvd -years $i
        done
        goval-dictionary fetch-${BENCHMARK_VULS_DISTRO} ${BENCHMARK_VULS_VERSIONS}
    fi
}

function test_run {
    # Create config.toml file on the log folder (per run, not shared)
    echo "[servers]" > ${LOGDIR}/config.toml
    echo "[servers.${BENCHMARK_VULS_DISTRO}]" >> ${LOGDIR}/config.toml
    echo "host        = \"${IPADDR}\"" >> ${LOGDIR}/config.toml
    echo "port        = \"${SSH_PORT}\"" >> ${LOGDIR}/config.toml
    echo "user        = \"${LOGIN}\"" >> ${LOGDIR}/config.toml
    echo "keyPath     = \"${SSH_KEY}\"" >> ${LOGDIR}/config.toml

    VULS_OPTIONSFLAG="-config=${LOGDIR}/config.toml -results-dir=${LOGDIR}"
    if [ "${BENCHMARK_VULS_ISDEEP}" = "true" ]; then
        VULS_DEEP_FLAG="-deep"
    fi

    cd $GOPATH/src/github.com/future-architect/vuls
    vuls scan ${VULS_DEEP_FLAG} ${VULS_OPTIONSFLAG} ${BENCHMARK_VULS_DISTRO}
    vuls report ${VULS_OPTIONSFLAG} -format-full-text | tee ${LOGDIR}/testlog.txt
}
