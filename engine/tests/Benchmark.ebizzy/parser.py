#!/usr/bin/python

import os, re, sys

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

ref_section_pat = "\[[\w]+.[gle]{2}\]"
cur_search_pat = re.compile("^([\d]+) (records\/s)")

cur_dict = {}
pat_result = plib.parse(cur_search_pat)
if pat_result:
	print pat_result
	cur_dict['ebizzy'] = pat_result[0][0]

sys.exit(plib.process_data(ref_section_pat, cur_dict, 's', 'records/sec'))
