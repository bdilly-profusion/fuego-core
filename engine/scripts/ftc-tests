This file has a list of tests for ftc, suitable for execution with clitest
run ftc-unit-test.sh to execute these tests

NOTE: when adding results to this, they must be preceded by exactly one tab.
vim and other editors may convert leading spaces into tab stops.
Much of the output of ftc is spaced-prefixed instead of tabbed.
So, don't do: 'ctrl-V (mark block) >' in VIM to indent a block,
Do this instead: 'ctrl-V (mark block) :s/^/\t/g'

Check some basic commands

Make sure board appears in the list of targets

	$ ftc targets | grep ftc-test
	    ftc-test
	$ ftc targets -q | grep ftc-test
	ftc-test
	$ ftc ftc-test info
	Warning: Missing base value to override for 'function_zzbar'
	Warning: Missing base value to override for 'ZZFOO'
	Information for target: ftc-test
	
	             ARCHITECTURE : "arm"
	                   DEVICE : "$IPADDR"
	  EXPAT_SUBTEST_COUNT_NEG : "41"
	  EXPAT_SUBTEST_COUNT_POS : "1769"
	               FUEGO_HOME : "/home/a"
	                   IPADDR : "192.168.7.2"
	                    LOGIN : "root"
	                  MMC_DEV : "/dev/mmcblk0p2"
	                   MMC_MP : "/mnt/mmc"
	                 PASSWORD : ""
	                     PATH : "/usr/local/bin:$PATH"
	                 PLATFORM : "fake-platform"
	                 SATA_DEV : "/dev/sdb1"
	                  SATA_MP : "/mnt/sata"
	                      SCP : "sshpass -e scp -o ServerAliveInterval=30 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o ConnectTimeout=30 -P $SSH_PORT"
	                   SRV_IP : "`/sbin/ip a s |awk -F ' +|/' '/inet / && $3 != "127.0.0.1" { print $3; exit; }'`"
	                      SSH : "sshpass -e ssh -o ServerAliveInterval=30 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o ConnectTimeout=30 -p $SSH_PORT $LOGIN@"
	                 SSH_PORT : "22"
	                TRANSPORT : "ssh"
	                  USB_DEV : "/dev/sda1"
	                   USB_MP : "/mnt/usb"
	                     ZFOO : "value2 (overridden)"
	                    ZZFOO : "not here"
	               board_path : /home/jenkins/overlays/boards/ftc-test.board
	function_ov_transport_cmd : function ov_transport_cmd() {
	                              case "$TRANSPORT" in
	                              "ssh")
	                                ${SSH}${DEVICE} "$@"
	                                ;;
	                              "serial")
	                                ;;
	                              *)
	                               abort_job "Error reason: TRANSPORT"
	                               ;;
	                              esac
	                            }
	                            
	function_ov_transport_get : function ov_transport_get () {
	                              case "$TRANSPORT" in
	                              "ssh")
	                                $SCP $LOGIN@${DEVICE}:"$1" "${*:2}"
	                                ;;
	                              "serial")
	                                ;;
	                              *)
	                               abort_job "Error reason: TRANSPORT"
	                               ;;
	                              esac
	                            }
	                            
	function_ov_transport_put : function ov_transport_put () {
	                              case "$TRANSPORT" in
	                              "ssh")
	                                $SCP "${@:1:$(($#-1))}" $LOGIN@$DEVICE:"${@: -1}"
	                                ;;
	                              "serial")
	                                ;;
	                              *)
	                               abort_job "Error reason: TRANSPORT"
	                               ;;
	                              esac  
	                            }
	                            
	            function_zbar : function zbar() {
	                            	echo "hello from zbar"
	                            	echo "misplaced brace"
	                               }
	                            
	            function_zfoo : function zfoo() {
	                            	echo "hello from zfoo (overridden)"
	                            	find . -type d
	                            }
	                            
	           function_zzbar : function zzbar() {
	                            	echo "in zzbar override"
	                            }
	                            

Get rid of those annoying errors:

	$ ftc ftc-test -v set ZZFOO="create base value"
	Warning: Missing base value to override for 'function_zzbar'
	Warning: Missing base value to override for 'ZZFOO'
	Set ZZFOO="create base value"
	$ ftc ftc-test -v set "function zzbar() {\n\techo zzbar\n}"
	Warning: Missing base value to override for 'function_zzbar'
	Set function zzbar="function zzbar() {\n\techo zzbar\n}"
	$ ftc ftc-test info -n function_zzbar
	function zzbar() {
		echo "in zzbar override"
	}
	

Try setting a base variable (SSH_PORT)

	$ ftc ftc-test info -n SSH_PORT
	"22"
	$ ftc ftc-test set SSH_PORT=5555
	$ ftc ftc-test info -n SSH_PORT
	"5555"

Check another base variable (ZFOO)

	$ ftc ftc-test info -n ZFOO
	"value2 (overridden)"
	$ ftc ftc-test set "ZFOO=value_1a (from set)"
	$ ftc ftc-test info -n ZFOO
	"value2 (overridden)"
	$ ftc ftc-test set "override ZFOO=value3 (from set)"
	$ ftc ftc-test info -n ZFOO
	"value3 (from set)"

Check that overrides are emitted without the 'override-func' prefix:

	$ ftc ftc-test info -n function_zfoo
	function zfoo() {
		echo "hello from zfoo (overridden)"
		find . -type d
	}
	

Add, remove, update some more functions:

	$ ftc ftc-test set "function znew() {\n\techo \"in znew\"\n}"
	$ ftc ftc-test info -n function_znew
	function znew() {
		echo "in znew"
	}
	
	$ ftc ftc-test set "function znew2() {\n\techo \"in znew2\"\n}"
	$ ftc ftc-test info -n function_znew2
	function znew2() {
		echo "in znew2"
	}
	
	$ ftc ftc-test -v delete function_znew2
	Deleted variable function_znew2
	$ ftc ftc-test set "function znew() {\n\techo \"in znew (updated)\"\n}"
	$ ftc ftc-test info -n function_znew
	function znew() {
		echo "in znew (updated)"
	}
	
Now try some setting and deleting some override variables:

	$ ftc ftc-test -v delete "override ZFOO"
	Deleted variable override ZFOO
	$ ftc ftc-test info -n ZFOO
	"value_1a (from set)"
	$ ftc ftc-test set "override ZFOO=value4 (from override test)"
	$ ftc ftc-test info -n ZFOO
	"value4 (from override test)"

Now try some setting and deleting some override functions:

#	$ ftc ftc-test -v delete "override-func zzbar"
#	Deleted variable override zzbar
#	$ ftc ftc-test info -n zzbar
#	function zzbar() {
#		echo zzbar
#	}
#	
#	$ ftc ftc-test set "override-func zzbar() {\n\techo \"zzbar override2\"\n}"
#	$ ftc ftc-test info -n zzbar
#	function zzbar() {
#		echo "zzbar override2"
#	}
	

Finish up

	$ echo "done"
	done
