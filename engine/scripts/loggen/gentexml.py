# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# DESCRIPTION
# This script generates TeXML file from testrun file

from lxml import etree
import os
import glob
import re
import argparse
import sys
import json
import string

log_lvl = 1

def debug_print(string, lev=1):
    if lev <= log_lvl:
        print "log: " + string


def load_logrun(path):
    with open(path) as f:
        jd = json.load(f)
        return jd


def create_gen_prolog():
    root = etree.Element('TeXML')
    doctype = etree.SubElement(root, "cmd", name="documentclass")

    opt = etree.SubElement(doctype, "opt")
    param = etree.SubElement(doctype, "parm")
    param.text = "article"
    opt.text = "12pt"

    mdoctype = etree.SubElement(root, "cmd", name="usepackage")

    mparam = etree.SubElement(mdoctype, "parm")
    mparam.text = "longtable"
    
    s = etree.tostring(root, pretty_print=True)
    # print ("prolog: %s" % (s))

    return root

def read_test_entries(lrs):
    test_entries = {}

    for lr in lrs:

        # put functional test results 
        if "testResult" in lr:
            test_entries[lr["testName"]] = {lr["testName"]: lr["testResult"]}
            continue

        lrf = lr["logFile"]

        if not os.path.isfile(lrf):
            erres = {lr["testName"]: "Test Error"}
            print ("%s log results file is not found. Skipping")

            test_entries[lr["testName"]] = erres
            continue
        
        with open(lrf) as f:
            lfjd = json.load(f)
            test_entries[lr["testName"]] = lfjd
            
    return test_entries

def put_col_align(root):
    spec = etree.SubElement(root, "spec", cat="align")

def put_nl(root):
    nl = etree.SubElement(root, "spec", cat="nl")

def put_col_esc(root):
    spec = etree.SubElement(root, "ctrl", ch="\\")
    nl = etree.SubElement(root, "spec", cat="nl")

def put_esc(root, text):
    spec = etree.SubElement(root, "spec", cat="esc")
    spec.tail =  "hline"

def put_text(root, text):
    group = etree.SubElement(root, "group")
    group.text = text

def put_group_named(root, text, gname):
    group = etree.SubElement(root, "group")
    cmd = etree.SubElement(group, "cmd", name=gname, gr="0")
    cmd.tail = text

def put_cmd(root, text, cmdname):
    cmd = etree.SubElement(root, "cmd", name=cmdname)
    parm = etree.SubElement(cmd, "parm")
    parm.text = text


def gen_table(root, test_entries, logrun, output_file):
    doc = etree.SubElement(root, "env", name="document")
    
    put_cmd(doc, "Test report %s " % (logrun["runNumber"]), "title")
    put_group_named(doc, "", "maketitle")

    run_info = etree.SubElement(doc, "env", name="itemize")
    
    put_group_named(run_info, "Device: %s" % logrun["device"], "item")
    put_group_named(run_info, "Test plan: %s" % logrun["testplan"], "item")

    # table = etree.SubElement(doc, "env", name="table")
    # placement = etree.SubElement(table, "opt")
    # placement.text = "h"

    tabular = etree.SubElement(doc, "env", name="longtable")

    rows = etree.SubElement(tabular, "parm")
    rows.text = "l l r"
    
    put_text(tabular, "Test name")
    put_col_align(tabular)
    put_text(tabular, "Metric")
    put_col_align(tabular)    
    put_text(tabular, "Results")
    put_col_esc(tabular)
    put_esc(tabular, "hline")

    for tr in test_entries:
        trr = test_entries[tr]
        put_text(tabular, tr)
        for m in trr:
            val = trr[m]

            put_col_align(tabular)
            put_text(tabular, m)
            put_col_align(tabular)
            put_text(tabular, val)
            put_col_esc(tabular)
        
        put_esc(tabular, "hline")

    s = etree.tostring(root, pretty_print=True)
    with open(output_file, "w+") as f:
        f.write(s)
    print ("%s" % (s))


def run(test_args=None):
    parser = argparse.ArgumentParser(description='Generate TeXML report from logrun file and test report files')

    parser.add_argument('--logrun-file', help='path to source logrun file', required=True)
    parser.add_argument('--output-file', help='write TeXML to this file', required=True)

    args = parser.parse_args(args=test_args)

    lr = load_logrun(args.logrun_file)
    prolog = create_gen_prolog()

    runLogs = lr["runLogs"]

    tes = read_test_entries(runLogs)
    
    gen_table(prolog, tes, lr, args.output_file)

def testrun():
    test_args = "--logrun-file minnow-logrun.json --output-file res.xml".split()
    run(test_args)

run()
# testrun()
