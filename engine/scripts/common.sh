# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# DESCRIPTION
# This script contains common utility functions


# prepend running python with ORIG_PATH if it exist
function run_python() {
    export RETURN_VALUE=$RETURN_VALUE
    if [ ! -z $ORIG_PATH ]
    then
        echo "running python with PATH=$ORIG_PATH"
        PATH=$ORIG_PATH PLATFORM=$PLATFORM python "$@"
    else
        python "$@"
    fi
}

function run_python_quiet() {
    if [ ! -z $ORIG_PATH ]
    then
        PATH=$ORIG_PATH python "$@"
    else
        python "$@"
    fi
}

function abort_job {
# $1 - Abort reason string

  set +x
  echo -e "\n*** ABORTED ***\n"
  [ -n "$1" ] && echo -e "Fuego error reason: $1\n"

  [ -n "$TARGET_TEARDOWN_LINK" ] && $TARGET_TEARDOWN_LINK || true
  ov_transport_disconnect || true

  # BUILD_URL should be defined. But if not, use a default
  if [ -z "$BUILD_URL" ] ; then
    BUILD_URL="http://localhost:8080/fuego/job/$JOB_NAME/$BUILD_NUMBER"
  fi
  wget -qO- "${BUILD_URL}/stop" > /dev/null
  sleep 15
  echo -e "Jenkins didn't stop the job - quit ourselves"

  # note that we don't send a job 'kill' operation to Jenkins
  # we might already be in post_test, but depending on the size
  # of the logs it might take a long time to retrieve them from the
  # target.  Just exit here, and let the signal handlers do their thing.
  exit 1
}

# check is variable is set and fail if otherwise
# $1 = variable to check
# $2 = optional message if variable is missing
function assert_define () {
    varname=$1
    if [ -z "${!varname}" ]
    then
        if [ -n "$2" ]  ; then
            msg="$2"
        else
            msg="Make sure you use the correct overlays and specs for this test/benchmark."
        fi
        abort_job "$1 is not defined. $msg"
    fi
}

# check if $1 starts with $2
function startswith {
    if [[ "${1:0:${#2}}" = "$2" ]]  ; then
        return 0
    fi
    return 1
}

# endswith - check if $1 ends with $2
function endswith {
	local len2=${#2}
	# check string length first
	if [[ ${#1} -lt $len2 ]] ; then
		return 1
	fi
	start=$(( ${#1} - $len2 ))
	if [ "${1:$start:$len2}" == "$2" ] ; then
		return 0
	fi
	return 1
}

# returns slice of $1, from $2 to $3
# accepts negative index to mean from end of string
function slice {
	local start=$2
	local end=$3

	len1=${#1}
	if [[ $start -lt 0 ]] ; then
		start=$(( $len1 + $start ))
		if [ $start -lt 0 ] ; then
			start=0
		fi
	fi

	if [[ $end -lt 0 ]] ; then
		end=$(( $len1 + $end ))
		if [[ $end -lt 0 ]] ; then
			end=0
		fi
	fi

	count=$(( $end - $start))
	if [[ $count -lt 0 ]] ; then
		count=0
	fi

	echo "${1:$start:$count}"
	return 0
}


TEST_HOME="$FUEGO_CORE/engine/tests/${TESTDIR}"
export JOB_BUILD_DIR="${JOB_NAME}-${PLATFORM}"
TEST_BUILD_DIR="${TESTDIR}-${PLATFORM}"
# FIXTHIS - fuego versions should get set at run time
# should use 'git describe' here
export FUEGO_VERSION="v1.2.1"
export FUEGO_CORE_VERSION="v1.2.1"

assert_define "FUEGO_CORE"
assert_define "FUEGO_RO"
assert_define "FUEGO_RW"

if [ -z "${BUILD_TIMESTAMP}" ] ; then
  export BUILD_TIMESTAMP=$(date +%FT%T%z)
fi
if [ -z "${FUEGO_START_TIME}" ] ; then
  start_arr=( $(date +"%s %N") )
  # save start time in milliseconds since the epoch
  # (convert nanoseconds returned by 'date' command to milliseconds)
  export FUEGO_START_TIME="${start_arr[0]}${start_arr[1]:0:3}"
fi
